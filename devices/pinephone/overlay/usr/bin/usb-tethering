#!/bin/bash

####
# Default profile
USB_IDVENDOR=0FCE
USB_IDPRODUCT=7169
USB_IPRODUCT="Unknown"
USB_ISERIAL="Unknown"
USB_IMANUFACTURER="GNU/Linux Device"
USB_IFACE="usb0"

####
# Override profile
if [ -f /etc/default/hybris-device ]; then
    source /etc/default/hybris-device
fi

USB_FUNCTIONS=rndis
LOCAL_IP=10.15.19.82
GADGET_DIR=/sys/kernel/config/usb_gadget

write() {
    echo -n "$2" > "$1"
}

# This sets up the USB with whatever USB_FUNCTIONS are set to via configfs
usb_setup_configfs() {
    G_USB_ISERIAL=$GADGET_DIR/g1/strings/0x409/serialnumber

    mkdir $GADGET_DIR/g1
    write $GADGET_DIR/g1/idVendor                   "0x$USB_IDVENDOR"
    write $GADGET_DIR/g1/idProduct                  "0x$USB_IDPRODUCT"
    mkdir $GADGET_DIR/g1/strings/0x409
    write $GADGET_DIR/g1/strings/0x409/serialnumber "$USB_ISERIAL"
    write $GADGET_DIR/g1/strings/0x409/manufacturer "$USB_IMANUFACTURER"
    write $GADGET_DIR/g1/strings/0x409/product      "$USB_IPRODUCT"

    if echo $USB_FUNCTIONS | grep -q "rndis"; then
        mkdir $GADGET_DIR/g1/functions/rndis.usb0
        mkdir $GADGET_DIR/g1/functions/rndis_bam.rndis
    fi
    echo $USB_FUNCTIONS | grep -q "mass_storage" && mkdir $GADGET_DIR/g1/functions/storage.0

    mkdir $GADGET_DIR/g1/configs/c.1
    mkdir $GADGET_DIR/g1/configs/c.1/strings/0x409
    write $GADGET_DIR/g1/configs/c.1/strings/0x409/configuration "$USB_FUNCTIONS"

    if echo $USB_FUNCTIONS | grep -q "rndis"; then
        ln -s $GADGET_DIR/g1/functions/rndis.usb0 $GADGET_DIR/g1/configs/c.1
        ln -s $GADGET_DIR/g1/functions/rndis_bam.rndis $GADGET_DIR/g1/configs/c.1
    fi
    echo $USB_FUNCTIONS | grep -q "mass_storage" && ln -s $GADGET_DIR/g1/functions/storage.0 $GADGET_DIR/g1/configs/c.1

    echo "$(ls /sys/class/udc)" > $GADGET_DIR/g1/UDC
}

# This determines which USB setup method is going to be used
usb_setup() {
    mount -t configfs none /sys/kernel/config || true
    usb_setup_configfs $1
}

usb_info() {
    sleep 1
    write $G_USB_ISERIAL "$1"
    echo "$1" >> /var/log/usb_info.log
}

ip_setup() {
    ip addr add 10.15.19.82/24 dev usb0
    ip link set dev usb0 up
    usb_info "$USB_IMANUFACTURER on $USB_IFACE $LOCAL_IP"
}

dhcpd_start() {
	INTERFACES="usb0"
	mkdir -p /run/hybris-usb
	touch /run/hybris-usb/dhcpd4.leases
	/usr/sbin/dhcpd -d -4 -cf /etc/hybris-usb/dhcpd.conf -lf /run/hybris-usb/dhcpd4.leases usb0
}

usb_setup
ip_setup
dhcpd_start

exit $?

